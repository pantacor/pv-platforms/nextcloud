FROM alpine as qemu

RUN if [ -n "arm" ]; then \
		wget -O /qemu-arm-static https://github.com/multiarch/qemu-user-static/releases/download/v4.0.0-5/qemu-arm-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-arm-static; \
	fi; \
	chmod a+x /qemu-arm-static

FROM arm32v5/nextcloud:stable-apache as orig

FROM scratch

COPY --from=qemu /qemu-arm-static /usr/bin/

COPY --from=orig / /

ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
ENV PHPIZE_DEPS autoconf dpkg-dev file g++ gcc libc-dev make pkg-config re2c
ENV PHP_INI_DIR /usr/local/etc/php
ENV APACHE_CONFDIR /etc/apache2
ENV APACHE_ENVVARS /etc/apache2/envvars
ENV PHP_EXTRA_BUILD_DEPS apache2-dev
ENV PHP_EXTRA_CONFIGURE_ARGS --with-apxs2 --disable-cgi
ENV PHP_CFLAGS="-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
ENV PHP_CPPFLAGS="-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
ENV PHP_LDFLAGS="-Wl,-O1 -Wl,--hash-style=both -pie"
ENV GPG_KEYS CBAF69F173A0FEA4B537F470D66C9593118BCCB6 F38252826ACD957EF380D39F2F7956BC5DA04B5D
ENV PHP_VERSION 7.3.11
ENV PHP_URL="https://www.php.net/get/php-7.3.11.tar.xz/from/this/mirror"
ENV PHP_ASC_URL="https://www.php.net/get/php-7.3.11.tar.xz.asc/from/this/mirror"
ENV PHP_SHA256="657cf6464bac28e9490c59c07a2cf7bb76c200f09cfadf6e44ea64e95fa01021"
ENV PHP_MD5=""
ENV NEXTCLOUD_VERSION 16.0.6

VOLUME /var/www/html

CMD [ "/entrypoint.sh", "apache2-foreground" ]
